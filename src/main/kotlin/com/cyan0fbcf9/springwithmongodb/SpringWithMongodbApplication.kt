package com.cyan0fbcf9.springwithmongodb

import org.springframework.beans.factory.annotation.Autowired
import org.springframework.boot.CommandLineRunner
import org.springframework.boot.autoconfigure.SpringBootApplication
import org.springframework.boot.runApplication

@SpringBootApplication
class SpringWithMongodbApplication: CommandLineRunner {
    @Autowired
    private lateinit var repository: CustomerRepository

    override fun run(vararg args: String?) {
        repository.deleteAll()

        // save customers
        repository.save(Customer("tadashi", "itou"))
        repository.save(Customer("ryou", "katou"))
        repository.save(Customer("tsubasa", "satou"))
        repository.save(Customer("yuuki", "itou"))
        repository.save(Customer("kiyo", "iida"))
        repository.save(Customer("ryou", "satou"))

        // fetch all customers
        println("Customers found with findAll():")
        println("-----------------")
        repository.findAll().forEach { customer ->
            println(customer)
        }
        println()

        // fetch an individual customer
        println("Customer found with findByFirstName('ryou')")
        println("----------------")
        println(repository.findByFirstName("ryou"))
        println()

        println("Customers found with findByLastName('itou')")
        println("----------------")
        repository.findByLastName("itou").forEach { customer ->
            println(customer)
        }
    }

}

fun main(args: Array<String>) {
    runApplication<SpringWithMongodbApplication>(*args)
}
