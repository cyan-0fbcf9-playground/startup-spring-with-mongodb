package com.cyan0fbcf9.springwithmongodb

import org.springframework.data.mongodb.repository.MongoRepository

interface CustomerRepository: MongoRepository<Customer, String> {
    fun findByFirstName(firstName: String): List<Customer>
    fun findByLastName(lastName: String): List<Customer>
}